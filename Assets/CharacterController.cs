﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.Analytics;

public class CharacterController : MonoBehaviour
{
    [SerializeField] private float m_speed = 5f;
    [SerializeField] private CinemachineFreeLook m_freelookCamera;
    private Rigidbody m_rigidBody;
    private Animator m_animatorController;
    private Vector2 m_axis;
    private static readonly int Blend = Animator.StringToHash("Blend");
    private bool m_stopped;
    private Quaternion m_forward;

    void Start()
    {
        m_rigidBody = GetComponent<Rigidbody>();
        m_animatorController = GetComponent<Animator>();
        Cursor.visible = false;
    }

    void Update()
    {
        m_axis = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
    }

    private void FixedUpdate()
    {
        if (Math.Abs(m_axis.x) < float.Epsilon && Math.Abs(m_axis.y) < float.Epsilon && m_stopped)
            return;

        Vector3 tempVect = new Vector3(m_axis.x, 0, m_axis.y);
        tempVect = tempVect.normalized;

        if (Math.Abs(m_axis.x) < float.Epsilon && Math.Abs(m_axis.y) < float.Epsilon)
        {
            m_stopped = true;
            m_animatorController.SetFloat(Blend, tempVect.normalized.magnitude);
            return;
        }

        Vector3 camF = GetCameraForward();
        Vector3 camR = GetCameraRight();
        camF.y = 0;
        camR.y = 0;
        camF = camF.normalized;
        camR = camR.normalized;

        m_stopped = false;
        m_animatorController.SetFloat(Blend, tempVect.normalized.magnitude);
        m_rigidBody.MovePosition(transform.position + (camF*tempVect.z+camR*tempVect.x)*m_speed);
        m_rigidBody.MoveRotation(Quaternion.LookRotation((camF*tempVect.z+camR*tempVect.x)));
    }

    private Vector3 GetCameraForward()
    {
        Vector3 l_forward = (transform.position - m_freelookCamera.transform.position).normalized;

        return l_forward;
    }

    private Vector3 GetCameraRight()
    {
        Vector3 l_forward = (transform.position - m_freelookCamera.transform.position).normalized;
       
        Vector3 l_right = new Vector3(l_forward.z, l_forward.y, -l_forward.x);

        return l_right;
    }

}
